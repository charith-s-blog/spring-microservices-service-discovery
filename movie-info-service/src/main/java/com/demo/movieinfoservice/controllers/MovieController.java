package com.demo.movieinfoservice.controllers;

import com.demo.movieinfoservice.models.Movie;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/movie")
public class MovieController {

    @RequestMapping("/{movieId}")
    public Movie getMovieInfo(@PathVariable("movieId") String movieId){
        if(movieId.equals("the-matrix")){
            return new Movie("the-matrix", "Enter The Matrix", "Neo getting unplugged");
        }else{
            return new Movie("the-matrix-reloaded", "The Matrix - Reloaded", "Frenchman is the shit");
        }
    }
}
